# PHP DEV task

Please write a Symfony application reading a text file containing YouTube channels, calculating and displaying their stats.

## Requirements:
- Your application will have 2 sides:
    - CLI command reading and processing given file as an argument
    - Simple front end page displaying the stats.
- The text file should contain 3-4 YouTube channel IDs to query.
- Upon reading a file all previously processed stats should be wiped out.
- A stats page will have a list of all channels and the following for each channel:
    - The average number of video views
    - Video name having the highest number of views with the link to it on YouTube.
- Your application should have a simple doc file with the instructions on how to run it

Using Symfony framework is a must. Using 3rd party libraries to help you communicate with relevant APIs is welcome.

## We're not trying to catch you out
Yet we want to see your best. If you feel you can not fully complete the task within a reasonable amount of time, send your best attempt anyway.

# LET'S GO!#
